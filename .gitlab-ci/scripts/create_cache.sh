#!/bin/bash
set -e

if [[ $CACHE_POLICY == "pull" ]] ; then
    echo Not creating cache due to cache policy $CACHE_POLICY
    exit 0
fi
CACHE_DIR=.gitlab-ci/cache
mkdir -p $CACHE_DIR
podman rm opencpi 2>/dev/null || true 
podman create --name opencpi ${1}
MNT=$(podman mount opencpi)
echo Extracting build
cp -r ${MNT}/home/user/opencpi/* ${CI_PROJECT_DIR}
cd ${CI_PROJECT_DIR}
# Gather all untracked changes from build
echo Creating cache
CACHE_PATHS=$(git ls-files --others --exclude-standard --ignored)
for f in $CACHE_PATHS; do if [[ -f $f ]]; then cp -d --parents $f $CACHE_DIR ; fi; done
podman unmount opencpi
podman rm opencpi
# Stamp the cache with the commit SHA of build
echo Making cache stamp with commit SHA $CI_COMMIT_SHA
echo ${CI_COMMIT_SHA} > $CACHE_DIR/.cachestamp
