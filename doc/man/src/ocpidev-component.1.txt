OCPIDEV-COMPONENT(1)
===================


NAME
----
ocpidev-component - manage an OpenCPI component.


SYNOPSIS
--------
*`ocpidev`* ['<options>'] '<verb>' *`component`* '<name>'


DESCRIPTION
-----------
The *`component`* noun represents an OpenCPI component, which is a defined
function that has 'properties' that configure and control it and 'ports'
where data messages are sent and received and which support 'protocols'
that define the allowed/expected messages.

A component is described by an OpenCPI Component Specification (OCS),
which is an XML file that defines the component's properties and ports
and usually references one or more OpenCPI Protocol Specifications (OPS)
in the port definitions. An OCS is also referred to as a "component spec" or "spec file".
The OCS establishes the interface requirements for all worker implementations
of the component in any authoring model. For more information about the
structure of an OCS, see the chapter "Component Specifications" in the
'OpenCPI Component Development Guide'.

A skeleton OpenCPI component asset document is also created along
with the component specification to allow for component documentation development
to occur during the overall component development process. Unless suppressed
by an option, building the component or the asset that contains it
builds viewable HTML output of the component asset document. The chapter
"Documenting OpenCPI Assets" in the 'OpenCPI Documentation Writer Guide'
provides more information about this feature and how to write a
component asset document.

In a project, a component and its asset document reside in
a '<name>'*`.comp`* directory located in a component library.
The component spec is named '<name>'*`-comp.xml`*.
The component asset document has the same name as the component spec
but with the *`.rst`* suffix instead of *`.xml`*.

In general, the workers that implement the component
and any component unit test suites created for the component reside
in the same library as the component, although a component's worker
implementations can be created in different libraries or even different projects.

Verbs that can operate on a component are:

*`create`*::
    Create the named component according to the specified options,
    creating directories and files as required.

*`delete`*::
    Remove all files associated with the named component.
    
*`show`*::
    Display information associated with a component, such as its defined
    properties and ports.

The *`ocpidev`* tool can also operate on a plural *`components`* noun.
The syntax is:

*`ocpidev`* ['<options>'] *`show components`*

where *`show`* is the only valid verb and the options that can be specified
are *`-d`* '<directory>' and the *`show`* verb options.

OPTIONS
-------
In addition to the options common to all OpenCPI tools (see link:opencpi.1.html[opencpi(1)]),
the options described below can be specified for operations on components.
In the following descriptions, a plus sign (+) after the option indicates
that it can be specified more than once on the command line.

Options That Apply to All Operations (Verbs) on Components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-h`* '<hdl-library>', *`--hdl-library=`*'<hdl-library>'::
    Specify the HDL library in which to operate on the component.
    HDL libraries exist underneath the *`hdl`*
    directory located at the top level of a project. Valid values
    are one of: *`devices`*, *`cards`*, *`adapters`*.
    See link:ocpidev-device.1.html[ocpidev-device(1)]
    for more information about this option.

*`-l`* '<component-library>', *`--library=`*'<component-library>'::
    Specify the component library in which to operate on the component.
    When a project contains multiple component libraries, this option
    can be used to specify one of them. Multiple component
    libraries are located underneath the *`components`* directory
    at the top level of a project.
   
Options When Creating Components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-k`*, *`--keep`*::
    Keep files and directories created after a component creation
    fails. Normally, all such files and directories are removed
    on any failure.

*`-n`*, *`--no-control`*::
   Specify that the component has no control interface. This option
   is rarely used or recommended, but is required for certain
   infrastructure components. It applies only to components
   with HDL-only implementations.
  
Options When Deleting Components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-f`*::
    Force deletion: do not ask for confirmation when deleting
    a component. Normally, you are asked to confirm a deletion.

Options When Showing Components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*`-P`* '<hdl-platform-directory>', *`--platform=`*'<hdl-platform-directory>'::
    Specify the HDL platform subdirectory in which to operate.

*--`global-scope`*::
    Show components from all registered projects and the
    current project if applicable.
    This is the default scope used if *`--local-scope`* is not used.

*`--json`*::
    Format the output in JavaScript Object Notation (JSON) format
    for integration with other software.

*`--local-scope`*::
    Only show components in the local project.

*`--simple`*::
    Format the output as simply as possible.

*`--table`*::
    Display the output in an easy-to-read table.
    This is the default display format
    used if *`--simple`* or *`--json`* are not used.
    
EXAMPLES
--------
. Create a skeleton component specification and component asset document for a
component named *`mycomp`*. The component specification and component asset document
are placed in the default location *`components/mycomp.comp/`*.
+
--------------------------------
ocpidev create component mycomp
--------------------------------
+
. Create a skeleton component specification and component asset document
named *`mycomp`* in the library named
*`myutilcomps`*. The component specification and the component asset document
are placed in the *`mycomp.comp`* directory in the *`myutilcomps`* sublibrary
within the *`components`* library (*`components/myutilcomps/mycomp.comp/`*):
+
-----------------------------------------------
ocpidev create component mycomp -l myutilcomps
-----------------------------------------------
+
. Delete all files for a component named *`mydevcomp`*
from the *`devices`* library underneath the project's *`hdl`* library
(deletes the directory *`hdl/devices/mydevcomp.comp/`*):
+
---------------------------------------------
ocpidev delete component mydevcomp -h devices
---------------------------------------------
+
. Inside the *`MyProject`* project, display information about
the component named *`mycomp`* in the 
*`myutilcomps`* sublibrary in the *`components`* library:
+
--------------------------------------------------------
ocpidev show component mycomp -l components/myutilcomps
--------------------------------------------------------
+
. Inside the *`assets`* project, display information about
the component named *`mydevcomp`* in the *`devices`* library
in the *`hdl`* library:
+
-------------------------------------------------
ocpidev show component mycomp -h devices
-------------------------------------------------
+
. Inside the *`assets`* project, display information about
all components that are local to the *`assets`* project:
+
--------------------------------------------
ocpidev show components --local-scope
-------------------------------------------
+
. Display information about all components local
to the project *`/home/workspace/MyProject`*:
+
------------------------------------------------------------------
ocpidev show components --local-scope  -d /home/workspace/MyProject
------------------------------------------------------------------

NOTES
-----
Component specifications created in earlier OpenCPI releases
reside in a *`specs/`* directory located either in a component library
or at the top level of a project and are named
'<name>'*`-spec.xml`* or '<name>'*`_spec.xml`*.

The following option to the *`ocpidev create`* operation for a component is
provided for backward compatibility with the component spec structure used
in earlier OpenCPI releases:

*`ocpidev create component`* '<name>' *`--file-only`*

The *`--file-only`* option directs *`ocpidev`* to create a component spec named
'<name>'*`-spec.xml`* in a *`specs/`* directory in a component library.
No component asset document is created.

BUGS
----
See https://www.opencpi.org/report-defects


RESOURCES
---------
See the main web site: https://www.opencpi.org

See the 'OpenCPI Component Development Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf

See the 'OpenCPI Documentation Writer Guide':
https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Documentation_Writer_Guide.pdf


SEE ALSO
--------
link:ocpidev.1.html[ocpidev(1)]
link:ocpidev-application.1.html[ocpidev-application(1)]
link:ocpidev-create.1.html[ocpidev-create(1)]
link:ocpidev-delete.1.html[ocpidev-delete(1)]
link:ocpidev-project.1.html[ocpidev-project(1)]
link:ocpidev-protocol.1.html[ocpidev-protocol(1)]
link:ocpidev-show.1.html[ocpidev-show(1)]
link:ocpidev-test.1.html[ocpidev-test(1)]
link:ocpidev-worker.1.html[ocpidev-worker(1)]

COPYING
-------
Copyright \(C) 2023 OpenCPI www.opencpi.org. OpenCPI is free software:
you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.
