/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/// @author Davis Hoover (git@davishoover.com)

#include "DRC.hh"
#include <cmath> // std::abs
#include <sstream>
#include <iomanip> // setprecision
#include "OcpiDebugApi.hh" // OS::Log::print

namespace OCPI {

namespace DRC {

DRC::DRC() : m_initialized(false) {
}

const Status&
DRC::get_status() const {
  throw_if_not_initialized();
  return m_status;
}

void
DRC::set_configuration(const uint16_t idx, const Configuration& config,
    bool force_set) {
  throw_if_not_initialized();
  const bool in_prep = m_status[idx].state == state_t::prepared;
  const bool in_oper = m_status[idx].state == state_t::operating;
  if ((in_prep || in_oper) && (!force_set)) {
    const char* m1 = "drc: deferring setting of configuration ";
    OS::Log::print(OCPI_LOG_INFO, "%s%i", m1, idx);
    m_deferred_configurations.emplace(idx, config);
  }
  else {
    OS::Log::print(OCPI_LOG_INFO, "drc: setting configuration %i", idx);
    if (m_configurations.find(idx) != m_configurations.end()) {
      m_configurations[idx] = config;
    }
    else {
      m_configurations.emplace(idx, config);
    }
    StatusConfiguration config2;
    config2.state = state_t::inactive;
    if (m_configurations.find(idx) != m_configurations.end()) {
      auto it = config.channels.begin();
      for (; it != config.channels.end(); ++it) {
        config2.channels.emplace(it->first, (Channel)it->second);
      }
    }
    if (m_status.find(idx) == m_status.end()) {
      m_status.emplace(idx, config2);
    }
    else {
      m_status.at(idx) = config2;
    }
  }
}

void
DRC::set_configuration_length(size_t val) {
  throw_if_not_initialized();
  for (size_t idx = m_configurations.size(); idx > val; idx--) {
    m_configurations.erase(idx);
  }
}

void
DRC::set_prepare(uint16_t val) {
  throw_if_not_initialized();
  throw_if_configuration_undefined(val);
  if (m_status[val].state != state_t::inactive) {
    if (m_status[val].state != state_t::error) {
      throw std::runtime_error("invalid state transition");
    }
  }
  check_and_handle_deferred_configuration(val);
  OS::Log::print(OCPI_LOG_INFO, "drc: preparing configuration %i", val);
  auto& config = m_configurations[val];
  std::string error;
  for (auto it = config.channels.begin(); it != config.channels.end(); ++it) {
    error.clear();
    for (auto itp = m_cache.begin(); itp != m_cache.end(); ++itp) {
      if (!it->second.rf_port_name.empty()) { // requesting by name
        if (itp->first.compare(it->second.rf_port_name)) {
          continue;
        }
      }
      error.assign(prepare_channel(val, it->first, itp->first, it->second));
      if (error.empty()) {
        break;
      }
    }
    auto name = it->second.rf_port_name.c_str();
    std::string msg = error.empty() ? "succeeded" : "failed";
    const char* m1 = "drc: preparing configuration ";
    const auto m2 = val;
    const char* m3 = " rf_port_name ";
    const auto DB = OCPI_LOG_DEBUG;
    OS::Log::print(DB, "%s%i%s%s %s", m1, m2, m3, name, msg.c_str());
    if (!error.empty()) {
      error = "no rf ports satisfy configuration (last fail: " + error + ")";
      if (m_configurations[val].recoverable) {
        m_status[val].error.assign(error);
        m_status[val].state = state_t::error;
        break;
      }
      else {
        throw std::runtime_error(error);
      }
    }
  }
  std::string msg = error.empty() ? "succeeded" : "failed";
  const char* m1 = "drc: preparing configuration ";
  OS::Log::print(OCPI_LOG_INFO, "%s%i %s", m1, val, msg.c_str());
  if (error.empty()) {
    m_status[val].state = state_t::prepared;
  }
}

void
DRC::set_start(uint16_t val) {
  throw_if_not_initialized();
  throw_if_configuration_undefined(val);
  check_and_handle_deferred_configuration(val);
  if (m_status[val].state != state_t::prepared) {
    set_prepare(val);
  }
  if (m_status[val].error.empty()) {
    const char* m1 = "drc: starting configuration ";
    OS::Log::print(OCPI_LOG_INFO, "%s%i", m1, val);
    auto& config = m_status.at(val);
    auto it = config.channels.begin();
    for (; it != config.channels.end(); ++it) {
      auto& channel = m_configurations[val].channels[it->first];
      start_channel(val, it->second.rf_port_name, it->second, channel);
    }
    m_status[val].state = state_t::operating;
    OS::Log::print(OCPI_LOG_INFO, "%s%i succeeded", m1, val);
  }
}

void
DRC::set_stop(uint16_t val) {
  throw_if_not_initialized();
  throw_if_configuration_undefined(val);
  if (m_status[val].state != state_t::operating) {
    throw std::runtime_error("invalid state transition");
  }
  OS::Log::print(OCPI_LOG_INFO, "drc: stopping configuration %i", val);
  m_status[val].state = state_t::prepared;
}

void
DRC::set_release(uint16_t val) {
  throw_if_not_initialized();
  throw_if_configuration_undefined(val);
  OS::Log::print(OCPI_LOG_INFO, "drc: releasing configuration %i", val);
  m_status[val].state = state_t::inactive;
}

void
DRC::initialize(const std::vector<std::string>& rf_ports_rx,
    const std::vector<std::string>& rf_ports_tx) {
  const auto& de = m_description;
  const auto LI = OCPI_LOG_INFO;
  for (auto it = rf_ports_rx.begin(); it != rf_ports_rx.end(); ++it) {
    m_cache.emplace(*it, Channel());
    const char* m1 = "drc: setting rf ";
    const char* m4 = " to allow rx";
    OS::Log::print(LI, "%s%s port named %s%s", m1, de.c_str(), it->c_str(), m4);
  }
  for (auto it = rf_ports_tx.begin(); it != rf_ports_tx.end(); ++it) {
    m_cache.emplace(*it, Channel());
    const char* m1 = "drc: setting rf ";
    const char* m4 = " to allow tx";
    OS::Log::print(LI, "%s%s port named %s%s", m1, de.c_str(), it->c_str(), m4);
  }
  OS::Log::print(OCPI_LOG_INFO, "drc: initializing rf port cache");
  // initialize the correct number (and names) of cached RF ports
  for (auto it = m_cache.begin(); it != m_cache.end(); ++it) {
    it->second.rx = get_rx(it->first);
    it->second.tuning_freq_MHz = get_tuning_freq_MHz(it->first);
    it->second.bandwidth_3dB_MHz = get_bandwidth_3dB_MHz(it->first);
    it->second.sampling_rate_Msps = get_sampling_rate_Msps(it->first);
    it->second.samples_are_complex = get_samples_are_complex(it->first);
    it->second.gain_mode = get_gain_mode(it->first);
    it->second.gain_dB = get_gain_dB(it->first);
    const char* port = it->first.c_str();
    const char* xx = (it->second.rx ? "1" : "0");
    const char* m1 = "drc: cache[";
    OS::Log::print(LI, "%s%s][rx]                  = %s", m1, port, xx);
    double val = it->second.tuning_freq_MHz;
    OS::Log::print(LI, "%s%s][tuning_freq_MHz]     = %f", m1, port, val);
    val = it->second.bandwidth_3dB_MHz;
    OS::Log::print(LI, "%s%s][bandwidth_3dB_MHz]   = %f", m1, port, val);
    val = it->second.sampling_rate_Msps;
    OS::Log::print(LI, "%s%s][sampling_rate_Msps]  = %f", m1, port, val);
    std::string sval = (it->second.samples_are_complex ? "true" : "false");
    OS::Log::print(LI, "%s%s][samples_are_complex] = %s", m1, port, sval.c_str());
    std::string& gm = it->second.gain_mode;
    OS::Log::print(LI, "%s%s][gain_mode]           = %s", m1, port, gm.c_str());
    val = it->second.gain_dB;
    OS::Log::print(LI, "%s%s][gain_dB]             = %f", m1, port, val);
    //const char* yy = (it->second.app_port_num ? "1" : "0");
    //OS::Log::print(LI, "drc: cache[%s][app_port_num]        = %s", port, yy);
  }
  m_initialized = true;
}

void
DRC::check_and_handle_deferred_configuration(uint16_t val) {
  if (m_deferred_configurations.find(val) != m_deferred_configurations.end()) {
    set_configuration(val, m_deferred_configurations[val], true);
    m_deferred_configurations.erase(val);
  }
}

/*! @param[in]    rf_port_name port being considered for the requested
 *                chan (chan.rf_port_name is allowed to be empty)
 *  @param[inout] channel configuration channel that is adjusted according to
 *                the tolerance and achievable operating plans
 *  @return string indicating error (empty if none) */
std::string
DRC::prepare_channel(uint16_t config_idx, uint16_t chan_idx,
    const std::string& rf_port_name, ConfigurationChannel& chan) {
  const auto& name = rf_port_name;
  auto& status_chan = m_status[config_idx].channels[chan_idx];
  OperatingPlan<bool> bool_plan;
  OperatingPlan<double> double_plan;
  OperatingPlan<std::string> string_plan;
  OperatingPlan<uint8_t> uint8_plan;
  for (auto it = m_status.begin(); it != m_status.end(); ++it) {
    const bool prep = it->second.state == state_t::prepared;
    const bool oper = it->second.state == state_t::operating;
    if (prep or oper) {
      auto it2 = it->second.channels.begin();
      for (; it2 != it->second.channels.end(); ++it2) {
        if (!(it2->second.rf_port_name.compare(rf_port_name))) {
          if (prep) {
            bool_plan.error.assign("rf port is already prepared");
          }
          else {
            bool_plan.error.assign("rf port is already operating");
          }
        }
      }
    }
  }

  const char* m1 = "drc: preparing configuration ";
  const auto& m2 = config_idx;
  const char* m3 = m_description.c_str();
  const char* m4 = " rf_port_name ";
  auto m5 = rf_port_name.c_str();
  const auto DB = OCPI_LOG_DEBUG;

  const auto no_plan_errors = [&]() {
    return
      bool_plan.error.empty()
      && double_plan.error.empty()
      && string_plan.error.empty()
      && uint8_plan.error.empty();
  };

  if (no_plan_errors()) {
    const bool val = chan.rx;
    const char* m6 = " rx with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%s", m1, m2, m3, m4, m5, m6, val ? "true" : "false");
    bool_plan = get_operating_plan_for_rx(chan, name, val);
    status_chan.rx = bool_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    const double val = chan.tuning_freq_MHz;
    const double tol = chan.tolerance_tuning_freq_MHz;
    const char* m6 = " tuning_freq_MHz with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%f", m1, m2, m3, m4, m5, m6, val);
    double_plan = get_operating_plan_for_tuning_freq_MHz(chan, name, val, tol);
    status_chan.tuning_freq_MHz = double_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    const double val = chan.bandwidth_3dB_MHz;
    const double tol = chan.tolerance_bandwidth_3dB_MHz;
    const char* m6 = " bandwidth_3dB_MHz with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%f", m1, m2, m3, m4, m5, m6, val);
    double_plan = get_operating_plan_for_bandwidth_3dB_MHz(chan, name, val, tol);
    status_chan.bandwidth_3dB_MHz = double_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    const double val = chan.sampling_rate_Msps;
    const double tol = chan.tolerance_sampling_rate_Msps;
    const char* m6 = " sampling_rate_Msps with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%f", m1, m2, m3, m4, m5, m6, val);
    double_plan = get_operating_plan_for_sampling_rate_Msps(chan, name, val, tol);
    status_chan.sampling_rate_Msps = double_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    const bool val = chan.samples_are_complex;
    const char* m6 = " samples_are_complex with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%s", m1, m2, m3, m4, m5, m6, val ? "true" : "false");
    bool_plan = get_operating_plan_for_samples_are_complex(chan, name, val);
    status_chan.samples_are_complex = bool_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    if (chan.gain_mode.compare("auto") && chan.gain_mode.compare("manual")) {
      string_plan.error.assign("requested unsupported gain mode");
      if (!m_configurations[config_idx].recoverable) {
        throw std::runtime_error(string_plan.error);
      }
    }
    else {
      const std::string val = chan.gain_mode;
      const char* m6 = " gain_mode with value ";
      OS::Log::print(DB, "%s%i %s%s%s%s%s", m1, m2, m3, m4, m5, m6, val.c_str());
      string_plan = get_operating_plan_for_gain_mode(chan, name, val);
      status_chan.gain_mode = string_plan.val_to_set_to;
    }
  }
  if (no_plan_errors() && (chan.gain_mode == "manual")) {
    const double val = chan.gain_dB;
    const double tol = chan.tolerance_gain_dB;
    const char* m6 = " gain_dB with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%f", m1, m2, m3, m4, m5, m6, val);
    double_plan = get_operating_plan_for_gain_dB(chan, name, val, tol);
    status_chan.gain_dB = double_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    const uint8_t val = chan.app_port_num;
    const char* m6 = " app_port_num with value ";
    OS::Log::print(DB, "%s%i %s%s%s%s%d", m1, m2, m3, m4, m5, m6, (int)val);
    uint8_plan = get_operating_plan_for_app_port_num(chan, name, val);
    status_chan.app_port_num = uint8_plan.val_to_set_to;
  }
  if (no_plan_errors()) {
    chan.rf_port_name = rf_port_name; // very important (request by name vs dir)
  }
  else {
    std::string err;
    if (!bool_plan.error.empty()) {
      err = bool_plan.error;
    }
    else if (!double_plan.error.empty()) {
      err = double_plan.error;
    }
    else if (!string_plan.error.empty()) {
      err = string_plan.error;
    }
    else if (!uint8_plan.error.empty()) {
      err = uint8_plan.error;
    }
    OS::Log::print(DB, "%s%i %s%s%s failed (%s)", m1, m2, m3, m4, m5, err.c_str());
    return err;
  }
  return "";
}

void
DRC::start_channel(uint16_t config_idx, const std::string& rf_port_name,
    const Channel& chan, const ConfigurationChannel& chan_for_tol) {

  const char* m1 = "drc: starting configuration ";
  const auto& m2 = config_idx;
  const char* m3 = m_description.c_str();
  const char* m4 = " rf_port_name ";
  const char* m5 = rf_port_name.c_str();
  const char* m7 = " with value ";
  const auto DB = OCPI_LOG_DEBUG;

  {
    const char* m6 = "rx";
    bool val = chan.rx;
    const char* s_val = val ? "true" : "false";
    OS::Log::print(DB, "%s%i %s%s%s %s%s%s", m1, m2, m3, m4, m5, m6, m7, s_val);
    if (m_cache[rf_port_name].rx != val) {
      val = set_rx(chan, rf_port_name, val);
      m_cache[rf_port_name].rx = val;
    }
  }
  {
    const char* m6 = "tuning_freq_MHz";
    double val = chan.tuning_freq_MHz;
    const double tol = chan_for_tol.tolerance_tuning_freq_MHz;
    OS::Log::print(DB, "%s%i %s%s%s %s%s%f", m1, m2, m3, m4, m5, m6, m7, val);
    if (std::abs(m_cache[rf_port_name].tuning_freq_MHz-val) > tol) {
      val = set_tuning_freq_MHz(chan, rf_port_name, val);
      m_cache[rf_port_name].tuning_freq_MHz = val;
    }
  }
  {
    const char* m6 = "bandwidth_3dB_MHz";
    double val = chan.bandwidth_3dB_MHz;
    const double tol = chan_for_tol.tolerance_bandwidth_3dB_MHz;
    OS::Log::print(DB, "%s%i %s%s%s %s%s%f", m1, m2, m3, m4, m5, m6, m7, val);
    if (std::abs(m_cache[rf_port_name].bandwidth_3dB_MHz-val) > tol) {
      val = set_bandwidth_3dB_MHz(chan, rf_port_name, val);
      m_cache[rf_port_name].bandwidth_3dB_MHz = val;
    }
  }
  {
    const char* m6 = "sampling_rate_Msps";
    double val = chan.sampling_rate_Msps;
    const double tol = chan_for_tol.tolerance_sampling_rate_Msps;
    OS::Log::print(DB, "%s%i %s%s%s %s%s%f", m1, m2, m3, m4, m5, m6, m7, val);
    if (std::abs(m_cache[rf_port_name].sampling_rate_Msps-val) > tol) {
      val = set_sampling_rate_Msps(chan, rf_port_name, val);
      m_cache[rf_port_name].sampling_rate_Msps = val;
    }
  }
  {
    const char* m6 = "samples_are_complex";
    bool val = chan.samples_are_complex;
    const char* s_val = val ? "true" : "false";
    OS::Log::print(DB, "%s%i %s%s%s %s%s%s", m1, m2, m3, m4, m5, m6, m7, s_val);
    if (m_cache[rf_port_name].samples_are_complex != val) {
      val = set_samples_are_complex(chan, rf_port_name, val);
      m_cache[rf_port_name].samples_are_complex = val;
    }
  }
  {
    const char* m6 = "gain_mode";
    std::string val = chan.gain_mode;
    const char* s_val = val.c_str();
    OS::Log::print(DB, "%s%i %s%s%s %s%s%s", m1, m2, m3, m4, m5, m6, m7, s_val);
    if (m_cache[rf_port_name].gain_mode != val) {
      val = set_gain_mode(chan, rf_port_name, val);
      m_cache[rf_port_name].gain_mode = val;
    }
  }
  {
    const char* m6 = "gain_dB";
    double val = chan.gain_dB;
    const double tol = chan_for_tol.tolerance_gain_dB;
    OS::Log::print(DB, "%s%i %s%s%s %s%s%f", m1, m2, m3, m4, m5, m6, m7, val);
    if (std::abs(m_cache[rf_port_name].gain_dB-val) > tol) {
      val = set_gain_dB(chan, rf_port_name, val);
      m_cache[rf_port_name].gain_dB = val;
    }
  }
  {
    const char* m6 = "app_port_num";
    uint8_t val = chan.app_port_num;
    const int i_val = (int)val;
    OS::Log::print(DB, "%s%i %s%s%s %s%s%d", m1, m2, m3, m4, m5, m6, m7, i_val);
    if (m_cache[rf_port_name].app_port_num != val) {
      val = set_app_port_num(chan, rf_port_name, val);
      m_cache[rf_port_name].app_port_num = val;
    }
  }
}

void
DRC::throw_invalid_rf_port_name(const std::string& rf_port_name) const {
  throw std::runtime_error(std::string("invalid rf_port_name: ")+rf_port_name);
}

/* @param[in] val       val to consider setting to, that could be adjusted
 *                      up to the tolerance amount
 * @param[in] low_limit absolute lower limit to be used for
 *                      val_to_set_to in the final plan (below this limit
 *                      results in an unachievable plan)
 * @param[in] hi_limit  absolute upper limit to be considered for
 *                      val_to_set_to in the final plan (above this limit
 *                      results in an unachievable plan)
 * @param[in] tolerance maximum value which is allowed to be subtracted
 *                      from or added to the considered plan's val_to_set_to
 *                      (outside this tolerance results in an unachievable
 *                      plan)
 * @return              final lock plan after limits are enforced */
OperatingPlan<double>
DRC::get_operating_plan_continuous_limits(double val,
    double low_limit, double hi_limit, double tolerance) {
  std::ostringstream oss;
  OperatingPlan<double> ret;
  ret.val_to_set_to = val;
  if ((val+tolerance) < low_limit) {
    oss << val << " below threshold of " << std::setprecision(10) << low_limit;
    ret.error.assign(oss.str());
  }
  else if ((val-tolerance) > hi_limit) {
    oss << val << " above threshold of " << std::setprecision(10) << hi_limit;
    ret.error.assign(oss.str());
  }
  else {
    double val = ret.val_to_set_to;
    ret.val_to_set_to = (val < low_limit) ? low_limit : val;
    val = ret.val_to_set_to;
    ret.val_to_set_to = (val > hi_limit) ? hi_limit : val;
  }
  return ret;
}

OperatingPlan<double>
DRC::get_operating_plan_discrete_val(double val,
    double achievable_val, double tolerance) {
  const auto& tol = tolerance;
  OperatingPlan<double> ret;
  ret.val_to_set_to = val;
  if ((achievable_val > (val-tol)) && (achievable_val < (val+tol))) {
    ret.val_to_set_to = achievable_val;
  }
  else {
    std::ostringstream oss;
    oss << std::setprecision(10) << val << " with tolerance " << tolerance;
    oss << " is not achievable but " << achievable_val << " is";
    ret.error.assign(oss.str());
  }
  return ret;
}

/* @param[in] val            val to consider setting to, that could be adjusted
 *                           up to the tolerance amount
 * @param[in] achievable_low lowest of 2 discrete possible values to be allowed
 *                           for val_to_set_to in the final plan (anything
 *                           other than the 2 results in an unachievable plan)
 * @param[in] achievable_hi  highest of 2 discrete possible values to be allowed
 *                           for val_to_set_to in the final plan (anything
 *                           other than the 2 results in an unachievable plan)
 * @param[in] tolerance      maximum value which is allowed to be subtracted
 *                           from or added to the considered plan's val_to_set_to
 *                           (outside this tolerance results in an unachievable
 *                           plan)
 * @return                   final operating plan after clamps are enforced */
OperatingPlan<double>
DRC::get_operating_plan_discrete_limits(double val,
    double achievable_low, double achievable_hi, double tolerance) {
  const auto& tol = tolerance;
  OperatingPlan<double> ret;
  ret.val_to_set_to = val;
  if ((achievable_low > (val-tolerance)) && (achievable_low < (val+tol))) {
    ret.val_to_set_to = achievable_low;
  }
  else if ((achievable_hi > (val-tolerance)) && (achievable_hi < (val+tol))) {
    ret.val_to_set_to = achievable_hi;
  }
  else {
    std::ostringstream oss;
    oss << val << " with tolerance " << tolerance << " is not achievable but ";
    oss << std::setprecision(10) << achievable_low << " and " << achievable_hi;
    oss << " are";
    ret.error.assign(oss.str());
  }
  return ret;
}

OperatingPlan<bool>
DRC::get_operating_plan_complex_samples_only(bool val) {
  OperatingPlan<bool> ret;
  ret.val_to_set_to = val;
  if (val) {
    ret.error.assign("real samples are not supported");
  }
  return ret;
}

OperatingPlan<std::string>
DRC::get_operating_plan_manual_gain_mode_only(std::string val) {
  OperatingPlan<std::string> ret;
  ret.val_to_set_to = val;
  if (val == "manual") {
    ret.error.assign("only the manual gain mode is supported");
  }
  return ret;
}

void
DRC::throw_if_configuration_undefined(uint16_t val) {
  if (m_status.find(val) == m_status.end()) {
    throw std::runtime_error("undefined configuration");
  }
}

void
DRC::throw_if_not_initialized() const {
  if (!m_initialized) {
    const char* m1 = "drc get... or set... method called before initialize()";
    throw std::runtime_error(m1);
  }
}

} // namespace DRC

} // namespace OCPI
